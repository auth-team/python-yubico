Source: python-yubico
Maintainer: Debian Authentication Maintainers <team+auth@tracker.debian.org>
Uploaders: Patrick Winnertz <winnie@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
	       dh-python,
	       python3-all (>= 2.6.6-3),
	       python3-setuptools (>= 0.6b3),
           python3-pytest,
	       python3-usb,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://developers.yubico.com/python-yubico/
Vcs-Browser: https://salsa.debian.org/auth-team/python-yubico
Vcs-Git: https://salsa.debian.org/auth-team/python-yubico.git
Testsuite: autopkgtest-pkg-python

Package: python-yubico-tools
Architecture: all
Section: utils
Depends: ${python3:Depends},
	 ${misc:Depends},
	 python3-yubico (= ${binary:Version})
Provides: ${python3:Provides}
Description: Tools for Yubico YubiKeys
 The YubiKey is a hardware authentication token. This package
 contains utilities for the YubiKey implemented using the
 python-yubico package.
 .
 This package currently includes the following utilities :
 .
   * yubikey-totp - OATH TOTP code generator using YubiKey

Package: python3-yubico
Architecture: all
Depends: ${python3:Depends},
	 ${misc:Depends},
	 python3-usb
Provides: ${python3:Provides}
Description: Python3 library for talking to Yubico YubiKeys
 The YubiKey is a hardware authentication token. This is a Python3
 library for interacting with YubiKeys. Typical use is to detect,
 configure (personalize) or issue challenge-responses to YubiKeys.
